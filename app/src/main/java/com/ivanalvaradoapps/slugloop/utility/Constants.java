package com.ivanalvaradoapps.slugloop.utility;

import com.google.android.gms.maps.model.LatLng;
import com.ivanalvaradoapps.slugloop.BusStop;

/**
 * Created by ivanalvarado on 3/21/18.
 */

public interface Constants {

    String BASE_URL = "http://bts.ucsc.edu:8081/location/";
    LatLng UCSC_LOCATION = new LatLng(36.9900, -122.0605);

    BusStop MAIN_ENTRANCE_OUTER    = new BusStop(36.977654, -122.053599, "Main Entrance", "Outer");
    BusStop LOWER_CAMPUS_OUTER     = new BusStop(36.981430, -122.051962, "Lower Campus", "Outer");
    BusStop LOWER_QUARRY_OUTER     = new BusStop(36.985851, -122.053511, "Lower Quarry Rd", "Outer");
    BusStop EAST_REMOTE_OUTER      = new BusStop(36.991276, -122.054696, "East Remote Parking", "Outer");
    BusStop EAST_FIELD_HOUSE_OUTER = new BusStop(36.994220, -122.055522, "East Field House", "Outer");
    BusStop BOOKSTORE_OUTER        = new BusStop(36.997455, -122.055066, "Bookstore", "Outer");
    BusStop CROWN_COLLEGE_OUTER    = new BusStop(36.999009, -122.055230, "Crown College", "Outer");
    BusStop COLLEGE_9_10_OUTER     = new BusStop(36.999901, -122.058372, "College 9 & 10", "Outer");
    BusStop SCIENCE_HILL_OUTER     = new BusStop(36.999913, -122.062371, "Science Hill", "Outer");
    BusStop KRESGE_COLLEGE_OUTER   = new BusStop(36.999225, -122.064522, "Kresge College", "Outer");
    BusStop COLLEGE_8_PORTER_OUTER = new BusStop(36.993011, -122.065332, "College 8/Porter", "Outer");
    BusStop FAMILY_STUDENT_OUTER   = new BusStop(36.991710, -122.066808, "Family Student Housing", "Outer");
    BusStop OAKES_COLLEGE_OUTER    = new BusStop(36.989923, -122.067298, "Oakes College", "Outer");
    BusStop ARBORETUM_OUTER        = new BusStop(36.983681, -122.065073, "Arboretum", "Outer");
    BusStop TOSCA_TERRACE_OUTER    = new BusStop(36.979882, -122.059269, "Tosca Terrace", "Outer");
    BusStop WESTERN_DR_OUTER       = new BusStop(36.978658, -122.057826, "Western Dr", "Outer");

    BusStop BARN_THEATER_INNER    = new BusStop(36.977317, -122.054255, "Barn Theater", "Inner");
    BusStop WESTER_DR_INNER       = new BusStop(36.978753, -122.057694, "Western Dr", "Inner");
    BusStop ARBORETUM_INNER       = new BusStop(36.982729, -122.062573, "Arboretum", "Inner");
    BusStop OAKES_COLLEGE_INNER   = new BusStop(36.990675, -122.066070, "Oakes College", "Inner");
    BusStop COLLEG_8_PORTER_INNER = new BusStop(36.992781, -122.064729, "College 8/Porter", "Inner");
    BusStop KERR_HALL_INNER       = new BusStop(36.996736, -122.063586, "Kerr Hall", "Inner");
    BusStop KRESGE_COLLEGE_INNER  = new BusStop(36.999198, -122.064371, "Kresge College", "Inner");
    BusStop SCIENCE_HILL_INNER    = new BusStop(36.999809, -122.062068, "Science Hill", "Inner");
    BusStop COLLEGE_9_10_INNER    = new BusStop(36.999719, -122.058393, "College 9 & 10", "Inner");
    BusStop BOOKSTORE_INNER       = new BusStop(36.996636, -122.055431, "Bookstore", "Inner");
    BusStop EAST_REMOTE_INNER     = new BusStop(36.991263, -122.054873, "East Remote Parking", "Inner");
    BusStop LOWER_QUARRY_INNER    = new BusStop(36.985438, -122.053505, "Lower Quarry Rd", "Inner");
    BusStop LOWER_CAMPUS_INNER    = new BusStop(36.981535, -122.052068, "Lower Campus", "Inner");
}
