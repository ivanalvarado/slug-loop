package com.ivanalvaradoapps.slugloop;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.ivanalvaradoapps.slugloop.model.BusInfo;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private final View mWindow;
    private Activity mContext;

    public CustomInfoWindowAdapter(Activity context) {
        mContext = context;
        mWindow = LayoutInflater.from(mContext).inflate(R.layout.custom_info_window, null);
//        CustomInfoWindowBinding binding = DataBindingUtil.setContentView(mContext, R.layout.custom_info_window);
//        binding.
    }

    private void renderWindowText(Marker marker, View view) {

        String title = "";
        String snippet = "";

        TextView tvTitle = view.findViewById(R.id.title_tv);
        TextView tvSnippet = view.findViewById(R.id.snippet_tv);
        TextView tvType = view.findViewById(R.id.type_tv);
        TextView tvDirection = view.findViewById(R.id.direction_tv);
        TextView tvNextStop = view.findViewById(R.id.next_stop_tv);

        RelativeLayout rL = view.findViewById(R.id.bus_custom_info_window_layout);
        LinearLayout lL = view.findViewById(R.id.bus_stop_custom_info_window_layout);

        if ((marker.getTag() != null) && (marker.getTag() instanceof BusInfo)) {
            rL.setVisibility(View.VISIBLE);
            lL.setVisibility(View.GONE);

            title = ((BusInfo) marker.getTag()).getType();
            String direction = ((BusInfo) marker.getTag()).getDirection();
            String nextStop = ((BusInfo) marker.getTag()).getNextStop();

            if (title != null && !title.equals("")) {
                tvType.setText(title);
            }

            if (direction != null && !direction.equals("")) {
                tvDirection.setText(direction);
            }

            if (nextStop != null && !nextStop.equals("")) {
                tvNextStop.setText(nextStop);
            }
        } else {
            lL.setVisibility(View.VISIBLE);
            rL.setVisibility(View.GONE);

            title = marker.getTitle();
            snippet = marker.getSnippet();

            if (title != null && !title.equals("")) {
                tvTitle.setText(title);
            }

            if (snippet != null && !snippet.equals("")) {
                tvSnippet.setText(snippet);
            }
        }
    }

    @Override
    public View getInfoWindow(Marker marker) {
        renderWindowText(marker, mWindow);
        return mWindow;
    }

    @Override
    public View getInfoContents(Marker marker) {
        renderWindowText(marker, mWindow);
        return mWindow;
    }
}
