package com.ivanalvaradoapps.slugloop.view;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.ArrayMap;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ivanalvaradoapps.slugloop.CustomInfoWindowAdapter;
import com.ivanalvaradoapps.slugloop.R;
import com.ivanalvaradoapps.slugloop.model.BusInfo;
import com.ivanalvaradoapps.slugloop.viewmodel.MapViewModel;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import io.reactivex.disposables.CompositeDisposable;

import static com.ivanalvaradoapps.slugloop.utility.Constants.UCSC_LOCATION;

/**
 * Created by ivanalvarado on 3/22/18.
 */

public class MapViewActivity extends FragmentActivity implements Observer, OnMapReadyCallback {

    private final String TAG = MapViewActivity.class.getSimpleName();

    private CompositeDisposable mDisposable;
    private MapViewModel mMapViewModel;
    private static GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_view);

        mMapViewModel = new MapViewModel(this);

        setupViews();
        setupObserver(mMapViewModel);
    }

    private void setupViews() {
        // Glue views to code or Data Bind?
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_view);
        mapFragment.getMapAsync(this);

    }

    private void setupObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(UCSC_LOCATION, 14));
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        bind();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unBind();
    }

    private void bind() {
        // Fetch bus data every 5 seconds
        final Handler handler = new Handler();
        final int delay = 5000; // 1000ms = 1s

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mMapViewModel.fetchBusData();
                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    private void unBind() {
        mDisposable.clear();
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof MapViewModel) {
            MapViewModel mapViewModel = (MapViewModel) observable;
            updateBusesOnMap(mapViewModel.getBusMarkerList());
        }
    }

    private void updateBusesOnMap(List<BusInfo> busList) {
        for (BusInfo bus : busList) {
            String busId = bus.getId();
            if (mMapViewModel.getMarkerArrayMap().containsKey(busId)) {
                mMapViewModel.updateBus(bus);
            } else {
                addBusToMap(bus);
            }
        }
    }

    private void addBusToMap(BusInfo bus) {
        Marker busMarker = mMap.addMarker(bus.getBusMarker());
        busMarker.setTag(bus);
        mMapViewModel.addMarkerToArrayMap(bus.getId(), busMarker);
    }
}
