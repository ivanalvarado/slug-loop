package com.ivanalvaradoapps.slugloop.viewmodel;

import android.content.Context;
import android.util.ArrayMap;
import android.util.Log;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ivanalvaradoapps.slugloop.datamodel.BusDataModel;
import com.ivanalvaradoapps.slugloop.model.BusData;
import com.ivanalvaradoapps.slugloop.model.BusInfo;
import com.ivanalvaradoapps.slugloop.network.RetrofitClient;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ivanalvarado on 3/21/18.
 */

public class MapViewModel extends java.util.Observable {

    private final String TAG = MapViewModel.class.getSimpleName();
    private BusDataModel mBusDataModel = new BusDataModel();
    private Context mContext;
    private List<BusInfo> mBusMarkerList;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public MapViewModel(Context context) {
        mContext = context;
    }

    public void fetchBusData() {
        Disposable disposable = RetrofitClient.getInstance()
                .getBuses()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<BusData>>() {
                    @Override
                    public void accept(List<BusData> busData) throws Exception {
                        updateBusList(busData);
                    }
                });

        mCompositeDisposable.add(disposable);
    }

    private void updateBusList(List<BusData> busData) {
        mBusMarkerList = mBusDataModel.buildBusMarkerList(busData);
        setChanged();
        notifyObservers();
    }

    public List<BusInfo> getBusMarkerList() {
        return mBusMarkerList;
    }

    public ArrayMap<String, Marker> getMarkerArrayMap() {
        return mBusDataModel.getMarkerArrayMap();
    }

    public void addMarkerToArrayMap(String id, Marker marker) {
        mBusDataModel.addMarkerToArrayMap(id, marker);
    }

    public void updateBus(BusInfo busInfo) {
        mBusDataModel.updateBus(busInfo);
    }

    private void unSubscribeFromObservable() {
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        mCompositeDisposable = null;
        mContext = null;
    }
}
