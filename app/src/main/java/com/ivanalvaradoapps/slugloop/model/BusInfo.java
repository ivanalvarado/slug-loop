package com.ivanalvaradoapps.slugloop.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class BusInfo {

    private String id;
    private String type;
    private String direction = "Determining";
    private String nextStop = "Determining";
    private MarkerOptions busMarker;
    private String eta;

    public BusInfo(String id, String type, LatLng position) {
        this.id = id;
        this.type = type;
        this.busMarker = new MarkerOptions()
                .position(position)
                .title(type);
    }

    public String getId() {
        return this.id;
    }

    public String getType() {
        return this.type;
    }

    public String getDirection() {
        return this.direction;
    }

    public MarkerOptions getBusMarker() {
        return this.busMarker;
    }

    public String getNextStop() {
        return this.nextStop;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void setNextStop(String nextStop) {
        this.nextStop = nextStop;
    }

}
