package com.ivanalvaradoapps.slugloop.model.service;

import com.ivanalvaradoapps.slugloop.network.BusApi;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ivanalvarado on 3/20/18.
 */

public class RestClient {

    private final String TAG = RestClient.class.getSimpleName();

    private final String BASE_URL = "http://bts.ucsc.edu:8081/location/";

    private BusApi mBusApi;

    public RestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        mBusApi = retrofit.create(BusApi.class);
    }

    public BusApi getBusApi() {
        return mBusApi;
    }
}
