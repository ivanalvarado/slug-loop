package com.ivanalvaradoapps.slugloop.datamodel;

import android.location.Location;
import android.util.ArrayMap;
import android.util.Log;

import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ivanalvaradoapps.slugloop.model.BusData;
import com.ivanalvaradoapps.slugloop.model.BusInfo;

import java.util.ArrayList;
import java.util.List;

import static com.ivanalvaradoapps.slugloop.utility.Constants.UCSC_LOCATION;

/**
 * Created by ivanalvarado on 3/22/18.
 */

public class BusDataModel {

    private final String TAG = BusDataModel.class.getSimpleName();

    private static ArrayMap<String, Marker> sMarkerArrayMap = new ArrayMap<>();

    /**
     * Convert the raw data into a usable structure of type BusInfo.
     * @param busData Raw data from Retrofit. JSON -> POJO via GSON/Jackson.
     * @return List of bus structures of type BusInfo.
     */
    public List<BusInfo> buildBusMarkerList(List<BusData> busData) {
        ArrayList<BusInfo> busList = new ArrayList<>();
        for (BusData bus : busData) {
            LatLng position = new LatLng(bus.getLat(), bus.getLon());
            BusInfo busMarker = new BusInfo(bus.getId(), bus.getType(), position);
            busList.add(busMarker);
        }
        return busList;
    }

    public void addMarkerToArrayMap(String id, Marker marker) {
        sMarkerArrayMap.put(id, marker);
    }

    public ArrayMap<String, Marker> getMarkerArrayMap() {
        return sMarkerArrayMap;
    }

    public void updateBus(BusInfo bus) {
        BusInfo oldBusInfo = (BusInfo) sMarkerArrayMap.get(bus.getId()).getTag();

        if (oldBusInfo != null) {
            LatLng oldPosition = sMarkerArrayMap.get(bus.getId()).getPosition();
            Double lat = bus.getBusMarker().getPosition().latitude;
            Double lon = bus.getBusMarker().getPosition().longitude;
            LatLng newPosition = new LatLng(lat, lon);
            String direction = determineBusDirection(oldPosition, newPosition, oldBusInfo.getDirection());

            oldBusInfo.setDirection(direction);

            sMarkerArrayMap.get(bus.getId()).setPosition(bus.getBusMarker().getPosition());
            sMarkerArrayMap.get(bus.getId()).setTag(oldBusInfo);

            // Refresh a custom info window if it is already showing.
            if (sMarkerArrayMap.get(bus.getId()) != null && sMarkerArrayMap.get(bus.getId()).isInfoWindowShown()) {
                sMarkerArrayMap.get(bus.getId()).showInfoWindow();
            }
        }
    }

    /**
     * I've split the map into 4 quadrants like the X-Y plane with the origin at the center
     * of the UCSC campus. This method determines in which quadrant each bus lies in. This is
     * needed in order to use the Unit Circle to aggregate the correct amount of degrees to the
     * calculated angle where 0 degrees occurs when the bus is on the positive x-axis. If the
     * bus lies on the positive y-axis, it's at 90 degrees. etc.
     * @param pos The current position of the bus as LatLng.
     * @return int: 1, 2, 3, or 4.
     */
    private int determineQuadrant(LatLng pos) {
        if (pos.latitude > 36.9900) {
            return pos.longitude > -122.0605 ? 1 : 2;
        } else {
            return pos.longitude > -122.0605 ? 4 : 3;
        }
    }

    /**
     * Determines what angle the current bus' position makes with respect to the origin and its
     * closest x,y-axis coordinate by using the Law of Cosines. This is needed to determine the
     * direction of the bus. If the new angle is greater than the old angle, the bus is
     * traveling counterclockwise around the campus from a top-down view. If the new angle is
     * less than the old angle, the bus is traveling clockwise from a top-down view.
     * @param toPos Bus' new position in LatLng.
     * @return The angle the new position makes as a float.
     */
    private float determineBusAngle(LatLng toPos) {
        LatLng axisPos = toPos;
        float angleAggregate = (float) 0.0;
        switch (determineQuadrant(toPos)) {
            case 1:
                axisPos = new LatLng(UCSC_LOCATION.latitude, -122.0471);
                angleAggregate = (float) 0.0;
                break;
            case 2:
                axisPos = new LatLng(37.0030, UCSC_LOCATION.longitude);
                angleAggregate = (float) 90.0;
                break;
            case 3:
                axisPos = new LatLng(UCSC_LOCATION.latitude, -122.0740);
                angleAggregate = (float) 180.0;
                break;
            case 4:
                axisPos = new LatLng(36.9732, UCSC_LOCATION.longitude);
                angleAggregate = (float) 270.0;
                break;
        }

        float[] d1 = new float[1];
        float[] d2 = new float[1];
        float[] d3 = new float[1];
        Location.distanceBetween(UCSC_LOCATION.latitude, UCSC_LOCATION.longitude, axisPos.latitude, axisPos.longitude, d1);
        Location.distanceBetween(UCSC_LOCATION.latitude, UCSC_LOCATION.longitude, toPos.latitude, toPos.longitude, d2);
        Location.distanceBetween(axisPos.latitude, axisPos.longitude, toPos.latitude, toPos.longitude, d3);

        float rad = ((d1[0]*d1[0]) + (d2[0]*d2[0]) - (d3[0]*d3[0]))/(2 * d1[0] * d2[0]);

        return ((float)Math.acos(rad) * (float)180.0)/((float)Math.PI) + angleAggregate;
    }

    /**
     * Determines whether a bus is "Inner" or "Outer" depending on it's last two positions.
     * @param oldPos Bus' last recorded position.
     * @param newPos Bus' new recorded position.
     * @param oldDirection A reference to the bus' old position. We need this just in case there
     *                     was an error calculating the bus' new angle from its new position.
     * @return "Inner" (Clockwise), "Outer" (Counterclockwise), Bus' old position, or "Determining"
     * if we still haven't been able to determine its position yet.
     */
    private String determineBusDirection(LatLng oldPos, LatLng newPos, String oldDirection) {
        float oldAngle = determineBusAngle(oldPos);
        float newAngle = determineBusAngle(newPos);

        Log.d(TAG, "Old Angle: " + oldAngle);
        Log.d(TAG, "New Angle: " + newAngle);

        String direction = oldDirection != null ? oldDirection : "Determining";

        if (newAngle > oldAngle) {
            direction = "Outer";
        } else if (oldAngle > newAngle) {
            direction = "Inner";
        }

        return direction;
    }

    /**
     * Calculates the distance bewteen point1 and point2 in meters.
     * @param point1 Point1 in question as LatLng.
     * @param point2 Point2 in question as LatLng.
     * @return Distance as a float in meters.
     */
    private float getDistanceBetween(LatLng point1, LatLng point2) {
        float[] distance = new float[1];
        Location.distanceBetween(point1.latitude, point1.longitude, point2.latitude, point2.longitude, distance);
        return distance[0];
    }

    /**
     * Checks if the bus is approaching the closest bus stop. This is necessary because there
     * are cases when the closest bus stop is the one the bus already visited.
     * @param busPos Bus' current position.
     * @param circle Center of the geofence created by this bus stop and it predecessor.
     * @return True is the bus is approaching this bus stop, False otherwise.
     */
    private boolean isBusBeforeBusStop(LatLng busPos, Circle circle) {
        LatLng circleCenter = new LatLng(circle.getCenter().latitude, circle.getCenter().longitude);
        return (getDistanceBetween(busPos, circleCenter) < circle.getRadius());
    }

//    private String getClosestBusStopToBus(String direction, LatLng busPos) {
//        float minDistance;
//        float currDistance;
//        int minIndex = 0;
//
//        if (direction.equals("Outer")) {
//            minDistance = getDistanceBetween(busPos, busStopsOuter.get(0).marker.getPosition());
//            for (int i = 0; i < busStopsOuter.size(); i++) {
//                currDistance = getDistanceBetween(busPos, busStopsOuter.get(i).marker.getPosition());
//                if (currDistance < minDistance) {
//                    minDistance = currDistance;
//                    minIndex = i;
//                }
//            }
//
//            if (!isBusBeforeBusStop(busPos, busStopsOuter.get(minIndex).getCircleBefore())) {
//                minIndex++; // We're past the last Outer Bus Stop in the Bus Stop List, reset
//                if (minIndex == 16) minIndex = 0;
//            }
//
//            return busStopsOuter.get(minIndex).marker.getTitle() + " (Outer)";
//        } else if (direction.equals("Inner")) {
//            minDistance = getDistanceBetween(busPos, busStopsInner.get(0).marker.getPosition());
//            for (int i = 0; i < busStopsInner.size(); i++) {
//                currDistance = getDistanceBetween(busPos, busStopsInner.get(i).marker.getPosition());
//                if (currDistance < minDistance) {
//                    minDistance = currDistance;
//                    minIndex = i;
//                }
//            }
//
//            if (!isBusBeforeBusStop(busPos, busStopsInner.get(minIndex).getCircleBefore())) {
//                minIndex++; // We're past the last Inner Bus Stop in the Bus Stop List, reset
//                if (minIndex == 13) minIndex = 0;
//            }
//
//            return busStopsInner.get(minIndex).marker.getTitle() + " (Inner)";
//        } else {
//            return "Determining";
//        }
//    }
}
