package com.ivanalvaradoapps.slugloop.network;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ivanalvaradoapps.slugloop.utility.Constants.BASE_URL;

/**
 * Created by ivanalvarado on 3/21/18.
 */

public class ApiFactory {

    public static BusApi create() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(BusApi.class);
    }

}
