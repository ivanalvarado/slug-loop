package com.ivanalvaradoapps.slugloop.network;

import com.ivanalvaradoapps.slugloop.model.BusData;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by ivanalvarado on 3/20/18.
 */

public interface BusApi {
    @GET("get")
    Observable<List<BusData>> getBuses();
}
