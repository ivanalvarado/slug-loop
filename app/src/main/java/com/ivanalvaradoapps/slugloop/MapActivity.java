package com.ivanalvaradoapps.slugloop;

import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ivanalvaradoapps.slugloop.model.BusInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String TAG = MapActivity.class.getSimpleName();
    private static String url = "http://bts.ucsc.edu:8081/location/get";
    private final boolean geofenceDebug = false;
    private static LatLng ucscCampusLocation = new LatLng(36.9900, -122.0605);
    private static GoogleMap mMap;
    private static ArrayMap<String, Marker> mMarkerList;
    private static ArrayList<BusStop> busStopsInner;
    private static ArrayList<BusStop> busStopsOuter;
    private Button mBusStopToggleBtn;
    private boolean isBusStopShowing = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mBusStopToggleBtn = findViewById(R.id.bus_stop_toggle_button);
        mBusStopToggleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isBusStopShowing) {
                    removeBusStopMarkers();
                } else {
                    addBusStops();
                }
                isBusStopShowing = !isBusStopShowing;
            }
        });

        mMarkerList = new ArrayMap<>();
        busStopsInner = new ArrayList<>();
        busStopsOuter = new ArrayList<>();

        // Fetch bus data every 5 seconds
        final Handler handler = new Handler();
        final int delay = 5000; // 1000ms = 1s

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new GetBusData().execute();

                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    BusStop mainEntranceOuter   = new BusStop(36.977654, -122.053599, "Main Entrance", "Outer");
    BusStop lowerCampusOuter    = new BusStop(36.981430, -122.051962, "Lower Campus", "Outer");
    BusStop lowerQuarryOuter    = new BusStop(36.985851, -122.053511, "Lower Quarry Rd", "Outer");
    BusStop eastRemoteOuter     = new BusStop(36.991276, -122.054696, "East Remote Parking", "Outer");
    BusStop eastFieldHouseOuter = new BusStop(36.994220, -122.055522, "East Field House", "Outer");
    BusStop bookstoreOuter      = new BusStop(36.997455, -122.055066, "Bookstore", "Outer");
    BusStop crownCollegeOuter   = new BusStop(36.999009, -122.055230, "Crown College", "Outer");
    BusStop college9n10Outer    = new BusStop(36.999901, -122.058372, "College 9 & 10", "Outer");
    BusStop scienceHillOuter    = new BusStop(36.999913, -122.062371, "Science Hill", "Outer");
    BusStop kresgeCollegeOuter  = new BusStop(36.999225, -122.064522, "Kresge College", "Outer");
    BusStop college8PorterOuter = new BusStop(36.993011, -122.065332, "College 8/Porter", "Outer");
    BusStop familyStudentOuter  = new BusStop(36.991710, -122.066808, "Family Student Housing", "Outer");
    BusStop oakesCollegeOuter   = new BusStop(36.989923, -122.067298, "Oakes College", "Outer");
    BusStop arboretumOuter      = new BusStop(36.983681, -122.065073, "Arboretum", "Outer");
    BusStop toscaTerraceOuter   = new BusStop(36.979882, -122.059269, "Tosca Terrace", "Outer");
    BusStop westernDrOuter      = new BusStop(36.978658, -122.057826, "Western Dr", "Outer");

    BusStop barnTheaterInner    = new BusStop(36.977317, -122.054255, "Barn Theater", "Inner");
    BusStop westernDrInner      = new BusStop(36.978753, -122.057694, "Western Dr", "Inner");
    BusStop arboretumInner      = new BusStop(36.982729, -122.062573, "Arboretum", "Inner");
    BusStop oakesCollegeInner   = new BusStop(36.990675, -122.066070, "Oakes College", "Inner");
    BusStop college8PorterInner = new BusStop(36.992781, -122.064729, "College 8/Porter", "Inner");
    BusStop kerrHallInner       = new BusStop(36.996736, -122.063586, "Kerr Hall", "Inner");
    BusStop kresgeCollegeInner  = new BusStop(36.999198, -122.064371, "Kresge College", "Inner");
    BusStop scienceHillInner    = new BusStop(36.999809, -122.062068, "Science Hill", "Inner");
    BusStop college9n10Inner    = new BusStop(36.999719, -122.058393, "College 9 & 10", "Inner");
    BusStop bookstoreInner      = new BusStop(36.996636, -122.055431, "Bookstore", "Inner");
    BusStop eastRemoteInner     = new BusStop(36.991263, -122.054873, "East Remote Parking", "Inner");
    BusStop lowerQuarryInner    = new BusStop(36.985438, -122.053505, "Lower Quarry Rd", "Inner");
    BusStop lowerCampusInner    = new BusStop(36.981535, -122.052068, "Lower Campus", "Inner");

    Marker mainEntranceOuterM;
    Marker lowerCampusOuterM;
    Marker lowerQuarryOuterM;
    Marker eastRemoteOuterM;
    Marker eastFieldHouseOuterM;
    Marker bookstoreOuterM;
    Marker crownCollegeOuterM;
    Marker college9n10OuterM;
    Marker scienceHillOuterM;
    Marker kresgeCollegeOuterM;
    Marker college8PorterOuterM;
    Marker familyStudentOuterM;
    Marker oakesCollegeOuterM;
    Marker arboretumOuterM;
    Marker toscaTerraceOuterM;
    Marker westernDrOuterM;

    Marker barnTheaterInnerM;
    Marker westernDrInnerM;
    Marker arboretumInnerM;
    Marker oakesCollegeInnerM;
    Marker college8PorterInnerM;
    Marker kerrHallInnerM;
    Marker kresgeCollegeInnerM;
    Marker scienceHillInnerM;
    Marker college9n10InnerM;
    Marker bookstoreInnerM;
    Marker eastRemoteInnerM;
    Marker lowerQuarryInnerM;
    Marker lowerCampusInnerM;

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ucscCampusLocation, 14));
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(MapActivity.this));

        addBusStops();
        buildCircleGeofences(geofenceDebug);
        buildBusStopLists();
    }

    /**
     * Adds bus stops as markers on the map.
     */
    private void addBusStops() {
        // Inner bus stops.
        mainEntranceOuterM   = mMap.addMarker(mainEntranceOuter.marker);
        lowerCampusOuterM    = mMap.addMarker(lowerCampusOuter.marker);
        lowerQuarryOuterM    = mMap.addMarker(lowerQuarryOuter.marker);
        eastRemoteOuterM     = mMap.addMarker(eastRemoteOuter.marker);
        eastFieldHouseOuterM = mMap.addMarker(eastFieldHouseOuter.marker);
        bookstoreOuterM      = mMap.addMarker(bookstoreOuter.marker);
        crownCollegeOuterM   = mMap.addMarker(crownCollegeOuter.marker);
        college9n10OuterM    = mMap.addMarker(college9n10Outer.marker);
        scienceHillOuterM    = mMap.addMarker(scienceHillOuter.marker);
        kresgeCollegeOuterM  = mMap.addMarker(kresgeCollegeOuter.marker);
        college8PorterOuterM = mMap.addMarker(college8PorterOuter.marker);
        familyStudentOuterM  = mMap.addMarker(familyStudentOuter.marker);
        oakesCollegeOuterM   = mMap.addMarker(oakesCollegeOuter.marker);
        arboretumOuterM      = mMap.addMarker(arboretumOuter.marker);
        toscaTerraceOuterM   = mMap.addMarker(toscaTerraceOuter.marker);
        westernDrOuterM      = mMap.addMarker(westernDrOuter.marker);

        // Outer bus stops.
        barnTheaterInnerM    = mMap.addMarker(barnTheaterInner.marker);
        westernDrInnerM      = mMap.addMarker(westernDrInner.marker);
        arboretumInnerM      = mMap.addMarker(arboretumInner.marker);
        oakesCollegeInnerM   = mMap.addMarker(oakesCollegeInner.marker);
        college8PorterInnerM = mMap.addMarker(college8PorterInner.marker);
        kerrHallInnerM       = mMap.addMarker(kerrHallInner.marker);
        kresgeCollegeInnerM  = mMap.addMarker(kresgeCollegeInner.marker);
        scienceHillInnerM    = mMap.addMarker(scienceHillInner.marker);
        college9n10InnerM    = mMap.addMarker(college9n10Inner.marker);
        bookstoreInnerM      = mMap.addMarker(bookstoreInner.marker);
        eastRemoteInnerM     = mMap.addMarker(eastRemoteInner.marker);
        lowerQuarryInnerM    = mMap.addMarker(lowerQuarryInner.marker);
        lowerCampusInnerM    = mMap.addMarker(lowerCampusInner.marker);
    }

    /**
     * Removes bus stops markers from the map.
     */
    private void removeBusStopMarkers() {
        mainEntranceOuterM.remove();
        lowerCampusOuterM.remove();
        lowerQuarryOuterM.remove();
        eastRemoteOuterM.remove();
        eastFieldHouseOuterM.remove();
        bookstoreOuterM.remove();
        crownCollegeOuterM.remove();
        college9n10OuterM.remove();
        scienceHillOuterM.remove();
        kresgeCollegeOuterM.remove();
        college8PorterOuterM.remove();
        familyStudentOuterM.remove();
        oakesCollegeOuterM.remove();
        arboretumOuterM.remove();
        toscaTerraceOuterM.remove();
        westernDrOuterM.remove();

        barnTheaterInnerM.remove();
        westernDrInnerM.remove();
        arboretumInnerM.remove();
        oakesCollegeInnerM.remove();
        college8PorterInnerM.remove();
        kerrHallInnerM.remove();
        kresgeCollegeInnerM.remove();
        scienceHillInnerM.remove();
        college9n10InnerM.remove();
        bookstoreInnerM.remove();
        eastRemoteInnerM.remove();
        lowerQuarryInnerM.remove();
        lowerCampusInnerM.remove();
    }

    /**
     * Calculate the midpoint between two LatLngs.
     * @param point1 First LatLng.
     * @param point2 Second LatLng.
     * @return Midpoint between two LatLngs as LatLng.
     */
    private LatLng getMidpoint(LatLng point1, LatLng point2) {

        double lat1 = (point1.latitude * Math.PI) / 180.0;
        double lat2 = (point2.latitude * Math.PI) / 180.0;
        double lon1 = (point1.longitude * Math.PI) / 180.0;
        double lon2 = (point2.longitude * Math.PI) / 180.0;

        double dLon = lon2 - lon1;

        double x = Math.cos(lat2) * Math.cos(dLon);
        double y = Math.cos(lat2) * Math.sin(dLon);

        double lat3 = Math.atan2( Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + x) * (Math.cos(lat1) + x) + y*y) );
        double lon3 = lon1 + Math.atan2(y, Math.cos(lat1) + x);

        double midPointLat = (lat3 * 180.0) / Math.PI;
        double midPointLon = (lon3 * 180.0) / Math.PI;

        return new LatLng(midPointLat, midPointLon);
    }

    /**
     * Gets the radius of a circle formed between point1 and point2.
     * @param point1 First point as LatLng.
     * @param point2 Second point as LatLng.
     * @return Radius of circle as double.
     */
    private double getRadiusOfMidpoint(LatLng point1, LatLng point2) {
        float[] distance = new float[1];
        Location.distanceBetween(point1.latitude, point1.longitude, point2.latitude, point2.longitude, distance);

        // Convert float to double, yikes!
        double doubleResult = Double.valueOf(Float.valueOf(distance[0]).toString());

        return doubleResult / 2.0;
    }

    /**
     * Builds circular geofences in between adjacent bus stops so we can determine if a bus
     * is BEFORE or AFTER a bus stop. Important for determining Next Bus Stop and ETAs accurately.
     */
    private void buildCircleGeofences(boolean value) {

        Circle between15n0 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(westernDrOuter.marker.getPosition(), mainEntranceOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(westernDrOuter.marker.getPosition(), mainEntranceOuter.marker.getPosition()))
        );
        between15n0.setVisible(value);
        Circle between0n1 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(mainEntranceOuter.marker.getPosition(), lowerCampusOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(mainEntranceOuter.marker.getPosition(), lowerCampusOuter.marker.getPosition()))
        );
        between0n1.setVisible(value);
        Circle between1n2 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(lowerCampusOuter.marker.getPosition(), lowerQuarryOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(lowerCampusOuter.marker.getPosition(), lowerQuarryOuter.marker.getPosition()))
        );
        between1n2.setVisible(value);
        Circle between2n3 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(lowerQuarryOuter.marker.getPosition(), eastRemoteOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(lowerQuarryOuter.marker.getPosition(), eastRemoteOuter.marker.getPosition()))
        );
        between2n3.setVisible(value);
        Circle between3n4 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(eastRemoteOuter.marker.getPosition(), eastFieldHouseOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(eastRemoteOuter.marker.getPosition(), eastFieldHouseOuter.marker.getPosition()))
        );
        between3n4.setVisible(value);
        Circle between4n5 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(eastFieldHouseOuter.marker.getPosition(), bookstoreOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(eastFieldHouseOuter.marker.getPosition(), bookstoreOuter.marker.getPosition()))
        );
        between4n5.setVisible(value);
        Circle between5n6 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(bookstoreOuter.marker.getPosition(), crownCollegeOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(bookstoreOuter.marker.getPosition(), crownCollegeOuter.marker.getPosition()))
        );
        between5n6.setVisible(value);
        Circle between6n7 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(crownCollegeOuter.marker.getPosition(), college9n10Outer.marker.getPosition()))
                .radius(getRadiusOfMidpoint(crownCollegeOuter.marker.getPosition(), college9n10Outer.marker.getPosition()))
        );
        between6n7.setVisible(value);
        Circle between7n8 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(college9n10Outer.marker.getPosition(), scienceHillOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(college9n10Outer.marker.getPosition(), scienceHillOuter.marker.getPosition()))
        );
        between7n8.setVisible(value);
        Circle between8n9 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(scienceHillOuter.marker.getPosition(), kresgeCollegeOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(scienceHillOuter.marker.getPosition(), kresgeCollegeOuter.marker.getPosition()))
        );
        between8n9.setVisible(value);
        Circle between9n10 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(kresgeCollegeOuter.marker.getPosition(), college8PorterOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(kresgeCollegeOuter.marker.getPosition(), college8PorterOuter.marker.getPosition()))
        );
        between9n10.setVisible(value);
        Circle between10n11 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(college8PorterOuter.marker.getPosition(), familyStudentOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(college8PorterOuter.marker.getPosition(), familyStudentOuter.marker.getPosition()))
        );
        between10n11.setVisible(value);
        Circle between11n12 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(familyStudentOuter.marker.getPosition(), oakesCollegeOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(familyStudentOuter.marker.getPosition(), oakesCollegeOuter.marker.getPosition()))
        );
        between11n12.setVisible(value);
        Circle between12n13 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(oakesCollegeOuter.marker.getPosition(), arboretumOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(oakesCollegeOuter.marker.getPosition(), arboretumOuter.marker.getPosition()))
        );
        between12n13.setVisible(value);
        Circle between13n14 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(arboretumOuter.marker.getPosition(), toscaTerraceOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(arboretumOuter.marker.getPosition(), toscaTerraceOuter.marker.getPosition()))
        );
        between13n14.setVisible(value);
        Circle between14n15 = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(toscaTerraceOuter.marker.getPosition(), westernDrOuter.marker.getPosition()))
                .radius(getRadiusOfMidpoint(toscaTerraceOuter.marker.getPosition(), westernDrOuter.marker.getPosition()))
        );
        between14n15.setVisible(value);

        mainEntranceOuter.setCircleBefore(between15n0);
        mainEntranceOuter.setCircleAfter(between0n1);
        lowerCampusOuter.setCircleBefore(between0n1);
        lowerCampusOuter.setCircleAfter(between1n2);
        lowerQuarryOuter.setCircleBefore(between1n2);
        lowerQuarryOuter.setCircleAfter(between2n3);
        eastRemoteOuter.setCircleBefore(between2n3);
        eastRemoteOuter.setCircleAfter(between3n4);
        eastFieldHouseOuter.setCircleBefore(between3n4);
        eastFieldHouseOuter.setCircleAfter(between4n5);
        bookstoreOuter.setCircleBefore(between4n5);
        bookstoreOuter.setCircleAfter(between5n6);
        crownCollegeOuter.setCircleBefore(between5n6);
        crownCollegeOuter.setCircleAfter(between6n7);
        college9n10Outer.setCircleBefore(between6n7);
        college9n10Outer.setCircleAfter(between7n8);
        scienceHillOuter.setCircleBefore(between7n8);
        scienceHillOuter.setCircleAfter(between8n9);
        kresgeCollegeOuter.setCircleBefore(between8n9);
        kresgeCollegeOuter.setCircleAfter(between9n10);
        college8PorterOuter.setCircleBefore(between9n10);
        college8PorterOuter.setCircleAfter(between10n11);
        familyStudentOuter.setCircleBefore(between10n11);
        familyStudentOuter.setCircleAfter(between11n12);
        oakesCollegeOuter.setCircleBefore(between11n12);
        oakesCollegeOuter.setCircleAfter(between12n13);
        arboretumOuter.setCircleBefore(between12n13);
        arboretumOuter.setCircleAfter(between13n14);
        toscaTerraceOuter.setCircleBefore(between13n14);
        toscaTerraceOuter.setCircleAfter(between14n15);
        westernDrOuter.setCircleBefore(between14n15);
        westernDrOuter.setCircleAfter(between15n0);

        Circle between12n0Inner = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(lowerCampusInner.marker.getPosition(), barnTheaterInner.marker.getPosition()))
                .radius(getRadiusOfMidpoint(lowerCampusInner.marker.getPosition(), barnTheaterInner.marker.getPosition()))
        );
        between12n0Inner.setVisible(value);
        Circle between0n1Inner = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(barnTheaterInner.marker.getPosition(), westernDrInner.marker.getPosition()))
                .radius(getRadiusOfMidpoint(barnTheaterInner.marker.getPosition(), westernDrInner.marker.getPosition()))
        );
        between0n1Inner.setVisible(value);
        Circle between1n2Inner = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(westernDrInner.marker.getPosition(), arboretumInner.marker.getPosition()))
                .radius(getRadiusOfMidpoint(westernDrInner.marker.getPosition(), arboretumInner.marker.getPosition()))
        );
        between1n2Inner.setVisible(value);
        Circle between2n3Inner = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(arboretumInner.marker.getPosition(), oakesCollegeInner.marker.getPosition()))
                .radius(getRadiusOfMidpoint(arboretumInner.marker.getPosition(), oakesCollegeInner.marker.getPosition()))
        );
        between2n3Inner.setVisible(value);
        Circle between3n4Inner = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(oakesCollegeInner.marker.getPosition(), college8PorterInner.marker.getPosition()))
                .radius(getRadiusOfMidpoint(oakesCollegeInner.marker.getPosition(), college8PorterInner.marker.getPosition()))
        );
        between3n4Inner.setVisible(value);
        Circle between4n5Inner = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(college8PorterInner.marker.getPosition(), kerrHallInner.marker.getPosition()))
                .radius(getRadiusOfMidpoint(college8PorterInner.marker.getPosition(), kerrHallInner.marker.getPosition()))
        );
        between4n5Inner.setVisible(value);
        Circle between5n6Inner = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(kerrHallInner.marker.getPosition(), kresgeCollegeInner.marker.getPosition()))
                .radius(getRadiusOfMidpoint(kerrHallInner.marker.getPosition(), kresgeCollegeInner.marker.getPosition()))
        );
        between5n6Inner.setVisible(value);
        Circle between6n7Inner = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(kresgeCollegeInner.marker.getPosition(), scienceHillInner.marker.getPosition()))
                .radius(getRadiusOfMidpoint(kresgeCollegeInner.marker.getPosition(), scienceHillInner.marker.getPosition()))
        );
        between6n7Inner.setVisible(value);
        Circle between7n8Inner = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(scienceHillInner.marker.getPosition(), college9n10Inner.marker.getPosition()))
                .radius(getRadiusOfMidpoint(scienceHillInner.marker.getPosition(), college9n10Inner.marker.getPosition()))
        );
        between7n8Inner.setVisible(value);
        Circle between8n9Inner = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(college9n10Inner.marker.getPosition(), bookstoreInner.marker.getPosition()))
                .radius(getRadiusOfMidpoint(college9n10Inner.marker.getPosition(), bookstoreInner.marker.getPosition()))
        );
        between8n9Inner.setVisible(value);
        Circle between9n10Inner = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(bookstoreInner.marker.getPosition(), eastRemoteInner.marker.getPosition()))
                .radius(getRadiusOfMidpoint(bookstoreInner.marker.getPosition(), eastRemoteInner.marker.getPosition()))
        );
        between9n10Inner.setVisible(value);
        Circle between10n11Inner = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(eastRemoteInner.marker.getPosition(), lowerQuarryInner.marker.getPosition()))
                .radius(getRadiusOfMidpoint(eastRemoteInner.marker.getPosition(), lowerQuarryInner.marker.getPosition()))
        );
        between10n11Inner.setVisible(value);
        Circle between11n12Inner = mMap.addCircle(new CircleOptions()
                .center(getMidpoint(lowerQuarryInner.marker.getPosition(), lowerCampusInner.marker.getPosition()))
                .radius(getRadiusOfMidpoint(lowerQuarryInner.marker.getPosition(), lowerCampusInner.marker.getPosition()))
        );
        between11n12Inner.setVisible(value);

        barnTheaterInner.setCircleBefore(between12n0Inner);
        barnTheaterInner.setCircleAfter(between0n1Inner);
        westernDrInner.setCircleBefore(between0n1Inner);
        westernDrInner.setCircleAfter(between1n2Inner);
        arboretumInner.setCircleBefore(between1n2Inner);
        arboretumInner.setCircleAfter(between2n3Inner);
        oakesCollegeInner.setCircleBefore(between2n3Inner);
        oakesCollegeInner.setCircleAfter(between3n4Inner);
        college8PorterInner.setCircleBefore(between3n4Inner);
        college8PorterInner.setCircleAfter(between4n5Inner);
        kerrHallInner.setCircleBefore(between4n5Inner);
        kerrHallInner.setCircleAfter(between5n6Inner);
        kresgeCollegeInner.setCircleBefore(between5n6Inner);
        kresgeCollegeInner.setCircleAfter(between6n7Inner);
        scienceHillInner.setCircleBefore(between6n7Inner);
        scienceHillInner.setCircleAfter(between7n8Inner);
        college9n10Inner.setCircleBefore(between7n8Inner);
        college9n10Inner.setCircleAfter(between8n9Inner);
        bookstoreInner.setCircleBefore(between8n9Inner);
        bookstoreInner.setCircleAfter(between9n10Inner);
        eastRemoteInner.setCircleBefore(between9n10Inner);
        eastRemoteInner.setCircleAfter(between10n11Inner);
        lowerQuarryInner.setCircleBefore(between10n11Inner);
        lowerQuarryInner.setCircleAfter(between11n12Inner);
        lowerCampusInner.setCircleBefore(between11n12Inner);
        lowerCampusInner.setCircleAfter(between12n0Inner);
    }

    private void buildBusStopLists() {
        busStopsOuter.add(0, mainEntranceOuter);
        busStopsOuter.add(1, lowerCampusOuter);
        busStopsOuter.add(2, lowerQuarryOuter);
        busStopsOuter.add(3, eastRemoteOuter);
        busStopsOuter.add(4, eastFieldHouseOuter);
        busStopsOuter.add(5, bookstoreOuter);
        busStopsOuter.add(6, crownCollegeOuter);
        busStopsOuter.add(7, college9n10Outer);
        busStopsOuter.add(8, scienceHillOuter);
        busStopsOuter.add(9, kresgeCollegeOuter);
        busStopsOuter.add(10, college8PorterOuter);
        busStopsOuter.add(11, familyStudentOuter);
        busStopsOuter.add(12, oakesCollegeOuter);
        busStopsOuter.add(13, arboretumOuter);
        busStopsOuter.add(14, toscaTerraceOuter);
        busStopsOuter.add(15, westernDrOuter);

        busStopsInner.add(0, barnTheaterInner);
        busStopsInner.add(1, westernDrInner);
        busStopsInner.add(2, arboretumInner);
        busStopsInner.add(3, oakesCollegeInner);
        busStopsInner.add(4, college8PorterInner);
        busStopsInner.add(5, kerrHallInner);
        busStopsInner.add(6, kresgeCollegeInner);
        busStopsInner.add(7, scienceHillInner);
        busStopsInner.add(8, college9n10Inner);
        busStopsInner.add(9, bookstoreInner);
        busStopsInner.add(10, eastRemoteInner);
        busStopsInner.add(11, lowerQuarryInner);
        busStopsInner.add(12, lowerCampusInner);
    }

    /**
     * Workhorse background thread that fetches bus data from Kerry's server.
     */
    private static class GetBusData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {

            HttpHandler sh = new HttpHandler();

            String response = sh.makeServiceCall(url);

            Log.e(TAG,"Response from url: " + response);

            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                try {
                    JSONArray buses = new JSONArray(result);
                    Log.e(TAG, "JSON Count: " + buses.length());

                    // If global structure mBusList is empty, fetching bus data for the first time.
                    if (mMarkerList.isEmpty()) {
                        for (int i = 0; i < buses.length(); i++) {
                            JSONObject bus = buses.getJSONObject(i);

                            Log.d(TAG, "id: " + bus.getString("id"));
                            Log.d(TAG, "lon: " + bus.getDouble("lon"));
                            Log.d(TAG, "lat: " + bus.getDouble("lat"));
                            Log.d(TAG, "type: " + bus.getString("type"));
                            Log.d(TAG, " ");

                            addBusToMap(bus);
                        }
                    } else {
                        // TODO: Update bus structure.
                        for (int i = 0; i < buses.length(); i++) {
                            JSONObject bus = buses.getJSONObject(i);
                            String busId = bus.getString("id");

                            if (mMarkerList.containsKey(busId)) {

                                BusInfo busInfo = (BusInfo) mMarkerList.get(busId).getTag();

                                if (busInfo != null) {

                                    // Determine bus' direction.
                                    LatLng oldPosition = mMarkerList.get(busId).getPosition();
                                    Double lat = bus.getDouble("lat");
                                    Double lon = bus.getDouble("lon");
                                    LatLng newPosition = new LatLng(lat, lon);
                                    String direction = determineBusDirection(oldPosition, newPosition, busInfo.getDirection());

                                    // Update bus info.
                                    busInfo.setDirection(direction);
                                    busInfo.setType(bus.getString("type"));
                                    busInfo.setNextStop(getClosestBusStopToBus(direction, newPosition));

                                    mMarkerList.get(busId).setPosition(newPosition);
                                    mMarkerList.get(busId).setTag(busInfo);
                                }

                                // Refresh a custom info window if it is already showing.
                                if (mMarkerList.get(busId) != null && mMarkerList.get(busId).isInfoWindowShown()) {
                                    mMarkerList.get(busId).showInfoWindow();
                                }
                            } else {
                                // TODO: Add it to global structures.
                                addBusToMap(bus);
                            }
                        }
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                }
            }
        }

        /**
         *
         * @param bus
         */
        private void addBusToMap(JSONObject bus) {
            try {
                String busId = bus.getString("id");

                LatLng position = new LatLng(bus.getDouble("lat"), bus.getDouble("lon"));
                MarkerOptions busMarker = new MarkerOptions()
                        .position(position)
                        .title(bus.getString("type"));

                Marker marker = mMap.addMarker(busMarker);

//                BusInfo busInfo = new BusInfo(busId, bus.getString("type"), "Determining");
//                marker.setTag(busInfo);
                mMarkerList.put(busId, marker);
            } catch (JSONException e) {
                Log.e(TAG, "Json parsing error: " + e.getMessage());
            }

        }

        /**
         * I've split the map into 4 quadrants like the X-Y plane with the origin at the center
         * of the UCSC campus. This method determines in which quadrant each bus lies in. This is
         * needed in order to use the Unit Circle to aggregate the correct amount of degrees to the
         * calculated angle where 0 degrees occurs when the bus is on the positive x-axis. If the
         * bus lies on the positive y-axis, it's at 90 degrees. etc.
         * @param pos The current position of the bus as LatLng.
         * @return int: 1, 2, 3, or 4.
         */
        private int determineQuadrant(LatLng pos) {
            if (pos.latitude > 36.9900) {
                return pos.longitude > -122.0605 ? 1 : 2;
            } else {
                return pos.longitude > -122.0605 ? 4 : 3;
            }
        }

        /**
         * Determines what angle the current bus' position makes with respect to the origin and its
         * closest x,y-axis coordinate by using the Law of Cosines. This is needed to determine the
         * direction of the bus. If the new angle is greater than the old angle, the bus is
         * traveling counterclockwise around the campus from a top-down view. If the new angle is
         * less than the old angle, the bus is traveling clockwise from a top-down view.
         * @param toPos Bus' new position in LatLng.
         * @return The angle the new position makes as a float.
         */
        private float determineBusAngle(LatLng toPos) {
            LatLng axisPos = toPos;
            float angleAggregate = (float) 0.0;
            switch (determineQuadrant(toPos)) {
                case 1:
                    axisPos = new LatLng(ucscCampusLocation.latitude, -122.0471);
                    angleAggregate = (float) 0.0;
                    break;
                case 2:
                    axisPos = new LatLng(37.0030, ucscCampusLocation.longitude);
                    angleAggregate = (float) 90.0;
                    break;
                case 3:
                    axisPos = new LatLng(ucscCampusLocation.latitude, -122.0740);
                    angleAggregate = (float) 180.0;
                    break;
                case 4:
                    axisPos = new LatLng(36.9732, ucscCampusLocation.longitude);
                    angleAggregate = (float) 270.0;
                    break;
            }

            float[] d1 = new float[1];
            float[] d2 = new float[1];
            float[] d3 = new float[1];
            Location.distanceBetween(ucscCampusLocation.latitude, ucscCampusLocation.longitude, axisPos.latitude, axisPos.longitude, d1);
            Location.distanceBetween(ucscCampusLocation.latitude, ucscCampusLocation.longitude, toPos.latitude, toPos.longitude, d2);
            Location.distanceBetween(axisPos.latitude, axisPos.longitude, toPos.latitude, toPos.longitude, d3);

            float rad = ((d1[0]*d1[0]) + (d2[0]*d2[0]) - (d3[0]*d3[0]))/(2 * d1[0] * d2[0]);

            return ((float)Math.acos(rad) * (float)180.0)/((float)Math.PI) + angleAggregate;
        }

        /**
         * Determines whether a bus is "Inner" or "Outer" depending on it's last two positions.
         * @param oldPos Bus' last recorded position.
         * @param newPos Bus' new recorded position.
         * @param oldDirection A reference to the bus' old position. We need this just in case there
         *                     was an error calculating the bus' new angle from its new position.
         * @return "Inner" (Clockwise), "Outer" (Counterclockwise), Bus' old position, or "Determining"
         * if we still haven't been able to determine its position yet.
         */
        private String determineBusDirection(LatLng oldPos, LatLng newPos, String oldDirection) {
            float oldAngle = determineBusAngle(oldPos);
            float newAngle = determineBusAngle(newPos);

            Log.d(TAG, "Old Angle: " + oldAngle);
            Log.d(TAG, "New Angle: " + newAngle);

            String direction = oldDirection != null ? oldDirection : "Determining";

            if (newAngle > oldAngle) {
                direction = "Outer";
            } else if (oldAngle > newAngle) {
                direction = "Inner";
            }

            return direction;
        }

        /**
         * Calculates the distance bewteen point1 and point2 in meters.
         * @param point1 Point1 in question as LatLng.
         * @param point2 Point2 in question as LatLng.
         * @return Distance as a float in meters.
         */
        private float getDistanceBetween(LatLng point1, LatLng point2) {
            float[] distance = new float[1];
            Location.distanceBetween(point1.latitude, point1.longitude, point2.latitude, point2.longitude, distance);
            return distance[0];
        }

        /**
         * Checks if the bus is approaching the closest bus stop. This is necessary because there
         * are cases when the closest bus stop is the one the bus already visited.
         * @param busPos Bus' current position.
         * @param circle Center of the geofence created by this bus stop and it predecessor.
         * @return True is the bus is approaching this bus stop, False otherwise.
         */
        private boolean isBusBeforeBusStop(LatLng busPos, Circle circle) {
            LatLng circleCenter = new LatLng(circle.getCenter().latitude, circle.getCenter().longitude);
            return (getDistanceBetween(busPos, circleCenter) < circle.getRadius());
        }

        private String getClosestBusStopToBus(String direction, LatLng busPos) {
            float minDistance;
            float currDistance;
            int minIndex = 0;

            if (direction.equals("Outer")) {
                minDistance = getDistanceBetween(busPos, busStopsOuter.get(0).marker.getPosition());
                for (int i = 0; i < busStopsOuter.size(); i++) {
                    currDistance = getDistanceBetween(busPos, busStopsOuter.get(i).marker.getPosition());
                    if (currDistance < minDistance) {
                        minDistance = currDistance;
                        minIndex = i;
                    }
                }

                if (!isBusBeforeBusStop(busPos, busStopsOuter.get(minIndex).getCircleBefore())) {
                    minIndex++; // We're past the last Outer Bus Stop in the Bus Stop List, reset
                    if (minIndex == 16) minIndex = 0;
                }

                return busStopsOuter.get(minIndex).marker.getTitle() + " (Outer)";
            } else if (direction.equals("Inner")) {
                minDistance = getDistanceBetween(busPos, busStopsInner.get(0).marker.getPosition());
                for (int i = 0; i < busStopsInner.size(); i++) {
                    currDistance = getDistanceBetween(busPos, busStopsInner.get(i).marker.getPosition());
                    if (currDistance < minDistance) {
                        minDistance = currDistance;
                        minIndex = i;
                    }
                }

                if (!isBusBeforeBusStop(busPos, busStopsInner.get(minIndex).getCircleBefore())) {
                    minIndex++; // We're past the last Inner Bus Stop in the Bus Stop List, reset
                    if (minIndex == 13) minIndex = 0;
                }

                return busStopsInner.get(minIndex).marker.getTitle() + " (Inner)";
            } else {
                return "Determining";
            }
        }
    }

}
