package com.ivanalvaradoapps.slugloop;

import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class BusStop {

    public MarkerOptions marker;
    private Circle beforeStop = null;
    private Circle afterStop = null;

    public BusStop(double lat, double lon, String title, String direc) {
        LatLng position = new LatLng(lat, lon);
        marker = new MarkerOptions()
                .position(position)
                .title(title)
                .snippet(direc);
    }

    public void setCircleBefore(Circle beforeStop) {
        this.beforeStop = beforeStop;
    }

    public void setCircleAfter(Circle afterStop) {
        this.afterStop = afterStop;
    }

    public Circle getCircleBefore() {
        return beforeStop;
    }

    public Circle getCircleAfter() {
        return afterStop;
    }
}
